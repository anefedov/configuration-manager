#pragma once

#include <vector>
#include <algorithm>
#include <string>
#include <initializer_list>

namespace config
{
	class registry
	{
	public:
		typedef std::vector< std::string > registry_type;
		typedef size_t					   param_id;

		static param_id add( const std::string & param_name ) throw (std::runtime_error)
		{
			registry_type::const_iterator it = 
				std::find( _registered_params.begin(), _registered_params.end(), param_name );
			if ( it != _registered_params.end() )
				throw std::runtime_error( "'" + param_name + "' is already added to a registry" );
			_registered_params.push_back( param_name );
			return _registered_params.size() - 1;
		}

		static const std::string & get( const param_id & param_id )
		{
			return _registered_params[param_id];
		}

	private:
		registry();
		registry(const registry&);
		registry& operator=(const registry&);

		static registry_type _registered_params;
	};

	registry::registry_type registry::_registered_params;

	class manager
	{
	public:
#if 0
		manager(std::initializer_list< registry::param_id > params_to_be_used )
		{}
#endif
		manager( const registry::param_id params_to_be_used[], size_t nb_params )
		{}

		template<typename T> T get( const registry::param_id & id ) throw (std::runtime_error)
		{ throw std::runtime_error("method is not implemented"); }

		template<> std::string get<std::string>( const registry::param_id & id ) throw (std::runtime_error)
		{
			return "sttring";
		}

	private:
		manager(const manager&);
		manager& operator=(const manager&);
	};
}
