#include <iostream>
#include "../src/registry.h"

auto Parameter1 = config::registry::add("MyFirstParameter");
auto Parameter2 = config::registry::add("MySecondParameter");
auto Parameter3 = config::registry::add("MyThirdParameter");

const config::registry::param_id params_to_be_used[] =
	{ Parameter1, Parameter3 };

config::manager my_config( params_to_be_used, 2 );

int main()
{
	//int p = my_config.get<int>(Parameter2);
	std::string st = my_config.get<std::string>( Parameter3 );
}